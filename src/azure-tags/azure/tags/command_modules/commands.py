# --------------------------------------------------------------------------------------------
# Copyright (c) Mason Huemmer. All rights reserved.
# Licensed under the MIT License. See License.txt in the project root for license information.
# --------------------------------------------------------------------------------------------

import os
import json
import timeit
import subprocess
from datetime import timedelta

from azure.tags.command_modules.log import get_logger

logger = get_logger(__name__)

def assign_tags():

    # Format string for pretty-print the command module table
    header_mod = "%-40s %10s %9s" % ( "Group Name", "Run Time", "Resources" )
    item_format_string = "%-40s %10.3f %9d"
    cumulative_format_string = "%-40s %-10.10s %9d"
    
    # limits, set to 0 to turn off
    groupLimit = 0
    resourceLimit = 0

    # counters
    count = 0
    cumulative_elapsed_time = 0
    cumulative_group_count = 0
    cumulative_resource_count = 0
    logger.info("Assigning Tags:")

    # return resource groups
    success, groupJsonString = get_group_list()

    if success: 

        groupJsonObject = json.loads(groupJsonString)

        for groupCount, groupKey in enumerate(groupJsonObject):

            if groupCount <= groupLimit or groupLimit == 0: 

                start_time = timeit.default_timer()

                groupId = groupKey["id"]

                # set group tag variables
                if "DepartmentName" in groupKey["tags"]:
                    groupDepartmentName = groupKey["tags"]["DepartmentName"]
                else:
                    groupDepartmentName = ""
                
                if "DepartmentTeam" in groupKey["tags"]:
                    groupDepartmentTeam = groupKey["tags"]["DepartmentTeam"]
                else:
                    groupDepartmentTeam = ""

                if "WorkloadName" in groupKey["tags"]:
                    groupWorkloadName = groupKey["tags"]["WorkloadName"]
                else:
                    groupWorkloadName = ""
                
                if "ApplicationName" in groupKey["tags"]:
                    groupApplicationName = groupKey["tags"]["ApplicationName"]
                else:
                    groupApplicationName = ""

                # set group tags
                success, result = set_resource_tags(groupId,groupDepartmentName,groupDepartmentTeam,groupWorkloadName,groupApplicationName)        

                if success:

                    # return resources 
                    success, resourceJsonString = get_resource_list(groupKey["name"])

                    if success: 

                        resourceJsonObject = json.loads(resourceJsonString)

                        for resourceCount, resourceKey in enumerate(resourceJsonObject):
                            
                            if resourceCount <= resourceLimit or resourceLimit == 0:
                                
                                # set resource properties
                                resourceId = resourceKey["id"]
                                resourceType = resourceKey["type"]

                                if resourceKey["tags"] is not None:

                                    # set resource department tag variables
                                    if "DepartmentName" in resourceKey["tags"]:
                                        resourceDepartmentName = resourceKey["tags"]["DepartmentName"]
                                    else:
                                        resourceDepartmentName = groupDepartmentName

                                    if "DepartmentTeam" in resourceKey["tags"]:
                                        resourceDepartmentTeam = resourceKey["tags"]["DepartmentTeam"]
                                    else:
                                        resourceDepartmentTeam = groupDepartmentTeam

                                    # set resource service tag variables
                                    if groupDepartmentName != resourceDepartmentName:
                                        resourceWorkloadName = resourceDepartmentTeam
                                        resourceApplicationName = resourceDepartmentTeam                
                                    else:
                                        resourceWorkloadName = groupWorkloadName
                                        resourceApplicationName = groupApplicationName

                                    # correct application name
                                    if resourceApplicationName == "Vendor Internal and Partnership":
                                        resourceApplicationName = "Vendor Inclusion and Partnership"
                                    
                                    # set resource tags
                                    success, result = set_resource_tags(resourceId,resourceDepartmentName,resourceDepartmentTeam,resourceWorkloadName,resourceApplicationName)

                                    if not success:
                                        continue

                        elapsed_time = timeit.default_timer() - start_time

                        if count == 0:
                            logger.info(header_mod)          
                        
                        count += 1
                        resourceCount += 1

                        logger.info(item_format_string,
                            groupKey["name"], elapsed_time, resourceCount)  

                        cumulative_group_count += groupCount
                        cumulative_resource_count += resourceCount
                        cumulative_elapsed_time += elapsed_time
        
        logger.info(cumulative_format_string,
                    "Total ({})".format(count), str(timedelta(seconds=cumulative_elapsed_time)), cumulative_resource_count)

def set_resource_tags(resourceId, departmentName, departmentTeam, workloadName, applicationName):

        commandList = [ "az", "tag", "update", 
            "--tags", "DepartmentName={0}".format(departmentName), "DepartmentTeam={0}".format(departmentTeam), "WorkloadName={0}".format(workloadName), "ApplicationName={0}".format(applicationName),
            "--resource-id", "{}".format(resourceId),
            "--operation", "merge", 
            "--output", "json" 
        ]
        commandString = " ".join([str(elem) for elem in commandList])
        logger.debug("system_command:\n'%s'", commandString)

        result = subprocess.run(commandList, capture_output=True, text=True)

        # IF STDERR
        if result.stderr and result.returncode:
            logger.error("\n'%s'", result.stderr)
            return False, result.stderr
        elif result.stderr:
            logger.warning(result.stderr)
            return True, result.stderr

        # IF STDOUT
        if result.stdout:
            return True, result.stdout

def get_resource_list(group_name=None):

    commandList = [ "az", "resource", "list", "--query", "[?resourceGroup == '{0}']".format(group_name), "--output", "json" ]
    commandString = " ".join([str(elem) for elem in commandList])
    logger.debug("system_command:\n'%s'", commandString)

    result = subprocess.run(commandList, capture_output=True, text=True)

    # IF STDERR
    if result.stderr and result.returncode:
        logger.error("\n'%s'", result.stderr)
        return False, result.stderr
    elif result.stderr:
        logger.warning(result.stderr)
        return True, result.stderr

    # IF STDOUT
    if result.stdout:
        return True, result.stdout

def get_group_list():

    commandList = [ "az", "group", "list", "--query", "[?tags.DepartmentName].{id:id, name:name, tags:tags}", "--output", "json" ]
    commandString = " ".join([str(elem) for elem in commandList])
    logger.debug("system_command:\n'%s'", commandString)

    result = subprocess.run(commandList, capture_output=True, text=True)

    # IF STDERR
    if result.stderr and result.returncode:
        logger.error("\n'%s'", result.stderr)
        return False, result.stderr
    elif result.stderr:
        logger.warning(result.stderr)
        return True, result.stderr

    # IF STDOUT
    if result.stdout:
        return True, result.stdout

