# --------------------------------------------------------------------------------------------
# Copyright (c) Mason Huemmer. All rights reserved.
# Licensed under the MIT License. See License.txt in the project root for license information.
# --------------------------------------------------------------------------------------------

from azure.tags.command_modules.log import get_logger
from azure.tags.command_modules.commands import assign_tags

logger = get_logger(__name__)

assign_tags()


